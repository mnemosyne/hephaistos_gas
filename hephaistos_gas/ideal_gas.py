"""
 Provide the IdealGas class
"""



##======================================================================================================================##
##                PACKAGES                                                                                              ##
##======================================================================================================================##

import argparse
import doctest

from measurement.measures import Temperature, Pressure, Volume


from hephaistos_gas.relations import rgas_from_molar_mass, molar_mass_from_rgas, basic_rho, basic_molar_mass #, n_from_m, m_from_v, v_from_m, m_from_n
from hephaistos_gas import relations



##======================================================================================================================##
##                CONSTANTS                                                                                             ##
##======================================================================================================================##

STD_EQ = "std"
ALT_EQ = "alt"
ALT_EQ2 = "alt2"

STD_VARS = list("pvnt")
ALT_VARS1 = ["p", "rho", "rgas", "t"]
ALT_VARS2 = ["p", "rho", "molar_mass", "t"]

DIC_EQS = {
    STD_EQ : STD_VARS,
    ALT_EQ : ALT_VARS1,
    ALT_EQ2 : ALT_VARS2
}

##======================================================================================================================##
##                FUNCTIONS                                                                                             ##
##======================================================================================================================##

def which_equation(*varnames):
    """
    Function doc
    >>> which_equation('n','r','t')
    Traceback (most recent call last):
        ...
    ValueError: Dont know which eq select wiht ('n', 'r', 't')
    >>> which_equation('n','p','t')
    'std'
    >>> which_equation('n','rho','t')
    Traceback (most recent call last):
        ...
    ValueError: Dont know which eq select wiht ('n', 'rho', 't')
    >>> which_equation('p','rho','t')
    'alt'
    >>> which_equation('p','molar_mass','t')
    'alt2'
    
    """
    res = None
    # ~ print(*varnames)
    for eq_nm, eq_vars in sorted(DIC_EQS.items()):
        # ~ print(eq_nm)
        req_match = len(eq_vars) - 1
        matches = [varnm for varnm in eq_vars if varnm in varnames]
        n_match = len(matches)
        # ~ print(matches)
        if n_match == req_match:
            # ~ print("match")
            res = eq_nm
            break
    
    if res is None:
        raise ValueError(f"Dont know which eq select wiht {varnames}")
        
    return res


def obtain_missing_parameter(eq_nm, **kwargs):
    """Set variables from kwargs and ideal gas equation (pv=nRt)"""
    res = kwargs.copy()
    
    if eq_nm == ALT_EQ2: #we have molar mass, then we need 'rgas'
        eq_nm = ALT_EQ
        assert "molar_mass" in res, f"pb code, retained eq must be {ALT_EQ}"
        res["rgas"] = rgas_from_molar_mass(res["molar_mass"])
        
    reqs = DIC_EQS[eq_nm]
    dic_req = {k: v for k, v in res.items() if k in reqs} #remove unnecessary parameters
    
    # ~ print(reqs, res)
    
    
    todef = [i for i in reqs if i not in res]
    assert len(todef) == 1, f"pb code {todef}"
    todef = todef[0]
    funcname = f"{todef}_from_{eq_nm}_idealgas_eq"
    func = getattr(relations, funcname)
    val = func(**dic_req)
    res[todef] = val
    
    
    if eq_nm == ALT_EQ: #possible that we do not have 'rgas' in the entries, then todef='rgas' and we have find it, we can now computre "molar mass"
        res["molar_mass"] = molar_mass_from_rgas(res['rgas'])
    
    return res


# ~ def from_idealgas_eq(**kwargs):
        # ~ """
        # ~ Set variables from kwargs and ideal gas equation (pv=nRt)
        # ~ """
        # ~ reqs = list("pvnt")
        # ~ dic_reqs = {k: v for k, v in kwargs.items() if k in reqs}
        #print(reqs, dic_reqs)
        # ~ todef = [i for i in reqs if i not in kwargs]
        # ~ assert len(todef) == 1, f"pb code {todef}"
        # ~ todef = todef[0]
        # ~ funcname = f"{todef}_from_idealgas_eq"
        # ~ func = getattr(relations, funcname)
        # ~ val = func(**dic_reqs)
        # ~ dic_reqs[todef] = val
            
        # ~ for key in sorted(dic_reqs.keys()):
            # ~ val = dic_reqs.pop(key)
            # ~ setattr(self, key, val)
            
        # ~ assert not dic_reqs
        
        # ~ return None

# ~ def from_alternative_eq(**kwargs):
    # ~ """
    # ~ Set variables from kwargs and alternative idealgas equation (p=rho * rgas * t)
    # ~ """
    # ~ reqs = ("p", "t", "rho")
    # ~ adds = ('m', 'n', 'v')

    # ~ assert self.molar_mass < 1, f"Molecular mass unit is kg/mol, {self.molar_mass} kg/mol seems too high"
    
    # ~ dic_reqs = {k: v for k, v in kwargs.items() if k in reqs}
    # ~ dic_adds = {k: v for k, v in kwargs.items() if k in adds}
    
    # ~ assert sorted(kwargs) == sorted(list(dic_reqs) + list(dic_adds)), f"remaing kwargs\ndont know what to do with : {kwargs}"
    
    # ~ ##TREAT REQS      
    # ~ todef = [i for i in reqs if i not in kwargs]
    # ~ assert len(todef) == 1, f"pb code {todef}"
    # ~ todef = todef[0]
    # ~ func = getattr(relations, f"{todef}_from_alt_idealgas_eq")
    #print(dic_reqs)
    # ~ val = func(rgas=self.rgas, **dic_reqs)
    # ~ dic_reqs[todef] = val
    
    # ~ for key in sorted(dic_reqs.keys()):
        # ~ val = dic_reqs.pop(key)
        # ~ setattr(self, key, val)
    
    # ~ assert not dic_reqs
    
    # ~ ##ADDS
    # ~ if dic_adds:
        # ~ assert len(dic_adds) == 1
        # ~ toadd = list(dic_adds)
        # ~ v2add = dic_adds[toadd]
        
        # ~ if toadd == 'n':
            # ~ n = v2add
            # ~ m = m_from_n(n=n, molar_mass=self.molar_mass)
            # ~ v = v_from_m(m=m, rho=self.rho)
            
        # ~ elif toadd == "m":
            # ~ m = v2add
            # ~ n = n_from_m(m=m, molar_mass=self.molar_mass)
            # ~ v = v_from_m(m=m, rho=self.rho)
        
        # ~ elif toadd == 'v':
            # ~ v = v2add 
            # ~ m = m_from_v(v=v, rho=self.rho)
            # ~ n = n_from_m(m=m, molar_mass=self.molar_mass)
        
        # ~ else:
            # ~ raise ValueError(f"Expect add in (n,m,v) got {toadd}")
        
        # ~ dic_adds = dict(m=m, n=n, v=v)
        # ~ for k in sorted(dic_adds.keys()):
            # ~ v = dic_adds.pop(k)
            # ~ setattr(self, k, v)
            
    # ~ assert not dic_reqs and not dic_adds, 'pb code'

##======================================================================================================================##
##                IDEAL GAS CLASSES                                                                                     ##
##======================================================================================================================##


class IdealGas:
    """ Parent class for gas
    https://f2school.com/gas-parfait-cours-exercices-corriges/

    >>> to2 = Temperature(k=300)
    >>> mmo2 = (2*16e-3)
    >>> po2 = Pressure(bar=1)
    >>> o2 = IdealGas(p=po2.pa, t=to2.k, molar_mass = mmo2)
    
    >>> round(o2.rgas, 1)
    259.7
    >>> round(o2.rho, 3)
    1.284
    >>> round(1/o2.rho, 3)
    0.779
    
    >>> print(o2)
    p          : 100000 (Pa)
    t          : 300.0 (K)
    rho        :  1.28 (kg/m^3)
    rgas       : 259.7 (J/kg/K)
    molar_mass : 0.032 (kg/mol)
    
    
    #https://new.kwyk.fr/exercices/physique-chimie/ts/thermodynamique/modele-des-gass-parfaits/
    
    >>> p = Pressure(bar=2.3)
    >>> t=Temperature(c=34)
    >>> v= Volume(l=30)
    >>> car_tire = IdealGas(p=p.pa, t=t.k, v=v.cubic_meter)
    
    >>> round(car_tire.n, 2)
    2.70
    
    >>> t2 = Temperature(c=14)
    >>> car_tire2 = IdealGas(t=t2.k, n=car_tire.n, v=v.cubic_meter)
    >>> round(car_tire2.p /1e5, 1)
    2.2
    
    #additional examples there :#https://new.kwyk.fr/exercices/physique-chimie/ts/thermodynamique/modele-des-gass-parfaits/
    
    >>> round(IdealGas(v=Volume(l=2.6e4).cubic_meter, n=48, t=Temperature(c=75.07).k).p, 1)
    5342.2
    >>> round(IdealGas(v=2.2, n=36, p=4.6e5).t, 1)
    3382.8
    >>> round(IdealGas(v=44, t=Temperature(c=7.37).k, p=9.3e5).n, 1)
    17553.8
    >>> round(IdealGas(n=16, t=Temperature(c=101.02).k, p=8.6e5).v, 4)
    0.0578
    
    """
    # ~ molar_mass = None #specific R of gas
    # ~ rgas = None #specific R of gas
    # ~ 
    UNITS = {"p" : "Pa", "t" : "K", 'rho' : 'kg/m^3', "m": "kg", "n": "mol", "v": "m^3", "rgas": "J/kg/K", "molar_mass": "kg/mol"}
    FMTS = {"p" : ".0f", "t" : ".1f", 'rho' : ".3g", "m": ".3g", "n": ".3g", "v": ".3g", "rgas": ".1f", "molar_mass": ".3g"}
    
    def __init__(self, equation_name=None, **kwargs):
        """ Class initialiser """
        #Variables
        self.p = None
        self.v = None
        self.n = None
        self.t = None
        self.rho = None
        self.m = None
        
        #CONSTANTS
        self.rgas = None
        self.molar_mass = None
        
        if equation_name is None:
            self.equation_name = which_equation(*kwargs.keys())
        else:
            self.equation_name = equation_name
            
        dic_to_set = obtain_missing_parameter(eq_nm=self.equation_name, **kwargs)
        for key in sorted(dic_to_set.keys()):
            assert hasattr(self, key), f"{key} is not expected"
            val = dic_to_set.pop(key)
            setattr(self, key, val)
    
        assert not dic_to_set
        
                
                
    def dic_pars(self):
        """
        Return dict = paramter:value
        """
        res = {k: v for k, v in self.__dict__.items() if k in self.UNITS}
        return res
        
        
            
    def who_is_there(self):
        """
        Get which key are defined
        """
        res = {k: v for k, v in self.dic_pars().items() if v is not None}
        return res
        
    def who_is_lacking(self):
        """
        Get which key are not defined
        """
        res = {k: v for k, v in self.dic_pars().items() if v is not None}
        return res
        
    def set_rgas(self, rgas):
        """
        Set r gas
        """
        assert self.equation_name == STD_EQ
        assert self.molar_mass is None
        assert self.rgas is None
        self.rgas = rgas
        self.molar_mass = molar_mass_from_rgas(rgas)
        
        return None
        
    def set_molar_mass(self, molar_mass):
        """
        set molar mass
        """
        assert self.equation_name == STD_EQ
        assert self.molar_mass is None
        assert self.rgas is None
        self.molar_mass = molar_mass
        self.rgas = rgas_from_molar_mass(molar_mass)
        
        return None
     
    def set_mass(self, m):
        """
        set mass
        """
        assert self.equation_name != STD_EQ
        assert self.m is None
        assert self.rho is None
        assert self.molar_mass is None
        assert self.rgas is None

        self.m = m
        self.rho = basic_rho(m=self.m, v=self.v)
        self.molar_mass = basic_molar_mass(n=self.n, m=self.m)
        self.rgas = rgas_from_molar_mass(molar_mass=self.molar_mass)
            
    
    def __str__(self):
        """Print the Object"""
        to_print = self.who_is_there()
        #~ units = ("K"   , "Pa"   , "kg/m3")
        
        
        txts = [f"{k:10} : {v:5{self.FMTS[k]}} ({self.UNITS[k]})" for k, v in to_print.items()]
        
        res = "\n".join(txts)
        return res


    
            
    

##======================================================================================================================##
##                MAIN                                                                                                  ##
##======================================================================================================================##

if __name__ == '__main__':
    
    #+++++++++++++++++++++++++++++++#
    #    DOCTESTS                   #
    #+++++++++++++++++++++++++++++++#
    parser = argparse.ArgumentParser(description='')
    parser.add_argument('--doctests', default=False, action='store_true')
    # ~ parser.add_argument('--example', default=False, action='store_true')
    opts = parser.parse_args()
    
    #+++++++++++++++++++++++++++++++#
    #    DOCTEST                    #
    #+++++++++++++++++++++++++++++++#
    if opts.doctests:
        test_result = doctest.testmod()
        print(test_result)


    #+++++++++++++++++++++++++++++++#
    #    EXAMPLE                    #
    #+++++++++++++++++++++++++++++++#
    
    #https://f2school.com/gas-parfait-cours-exercices-corriges/
    
    to2 = Temperature(k=300)
    mmo2 = (2*16e-3)
    po2 = Pressure(bar=1)
    o2 = IdealGas(p=po2.pa, t=to2.k, molar_mass = mmo2)
    print(o2.rgas)
    print(o2.rho)
    print(1/o2.rho)

    
    p_tire = Pressure(bar=2.3)
    t_tire=Temperature(c=34)
    v_tire= Volume(l=30)

    car_tire = IdealGas(p=p_tire.pa, t=t_tire.k, v=v_tire.cubic_meter)
    
    print(car_tire)


    # ~ from japet_misc import todo_msg
    # ~ print(todo_msg("trouver exercice + coorection", __file__))
    # ~ print(todo_msg("voir tableau periodic.", __file__))
