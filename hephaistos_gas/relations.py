"""
 Some relations for gas
 See IdealGas class for doctests
"""



##======================================================================================================================##
##                PACKAGES                                                                                              ##
##======================================================================================================================##

import argparse
import doctest

from apollon_physics import R_STAR


##======================================================================================================================##
##                CONSTANTS                                                                                             ##
##======================================================================================================================##




##======================================================================================================================##
##                FUNCTIONS                                                                                             ##
##======================================================================================================================##

def rgas_from_molar_mass(molar_mass):
    """
    Specific gas constant: J/kg/K
    """
    res = R_STAR / molar_mass
    return res

def molar_mass_from_rgas(rgas):
    """
    Molar mass: kg / mol
    """
    res = R_STAR / rgas
    return res



#ALTERNATIVE EQ#
#---------------#
# p = rho rgaz t

def t_from_alt_idealgas_eq(p, rgas, rho):
    """Ideal Gas temperature: K"""
    res = p / (rgas * rho)
    return res
    
def p_from_alt_idealgas_eq(rgas, rho, t):
    """Ideal Gas pressure : Pa"""
    res = rgas * rho * t
    return res

def rho_from_alt_idealgas_eq(p, rgas, t):
    """rho gas : kg/m3"""
    res = p / (rgas * t)
    return res

def rgas_from_alt_idealgas_eq(p, rho, t):
    """rho gas : kg/m3"""
    res = p / (rho * t)
    return res

#IDEAL GAS EQ#
#---------------#

#p * v = n R* T

def t_from_std_idealgas_eq(p, v, n):
    """Temperature from ideal gas equation
    unit: K
    """
    res = (p*v) / (n* R_STAR)
    return res
    

def v_from_std_idealgas_eq(p, t, n):
    """Volume from ideal gas equation 
    unit: m^3
    """
    res = (n * R_STAR * t) / p
    return res


def p_from_std_idealgas_eq(t, v, n):
    """ Pressure from ideal gas equation
    unit: pa
    """
    res = (n * R_STAR * t) / v
    return res

def n_from_std_idealgas_eq(p, v, t):
    """
    Substance quantity from ideal gas equation
    unit: mol
    >>> round(n_from_std_idealgas_eq(p=2.3e5, v=30e-3, t=273.15+34), 2)
    2.7
    """
    res = (p*v) / (R_STAR * t)
    return res
    
#BASIC RELATIONS#
#---------------#
 
def basic_rho(m,v):
    """Basic computation of volumic mass
    rho gas : kg/m3
    """
    res = m/v
    return res
    
def basic_molar_mass(n,m):
    """ Basic computation of Molar mass from n and m
    M: mol/kg
    """
    res = m / n
    return res
    
def n_from_m(m, molar_mass):
    """Substance quantity: mol"""
    res =  m / molar_mass
    return res

def m_from_v(v, rho):
    """Mass of gas: kg"""
    res = rho * v
    return res

def m_from_n(n, molar_mass):
    """Mass of gas: kg"""
    res = molar_mass * n
    return res
        
def v_from_m(m, rho):
    """Volume: m^3"""
    res = m / rho
    return res


        

# ~ def who_lacks(*varnames, eq_nm):
    # ~ """
    # ~ >>> who_lacks('p','rho','t', eq_nm="alt")
    
    # ~ >>> who_lacks('p','v','t', eq_nm="std")
    # ~ """
    # ~ exp_vars = DIC_EQS[eq_nm]
    # ~ res = [var for var in exp_vars if var not in varnames]
    # ~ assert len(res) == 1, f"dont see who lacks: {res=}\n{varnames=}\n{exp_vars=}"
    # ~ res = res[0]
    # ~ return res


##======================================================================================================================##
##                MAIN                                                                                                  ##
##======================================================================================================================##

if __name__ == '__main__':
    
    #+++++++++++++++++++++++++++++++#
    #    DOCTESTS                   #
    #+++++++++++++++++++++++++++++++#
    parser = argparse.ArgumentParser(description='')
    parser.add_argument('--doctests', default=False, action='store_true')
    opts = parser.parse_args()
    
    #+++++++++++++++++++++++++++++++#
    #    DOCTEST                    #
    #+++++++++++++++++++++++++++++++#
    if opts.doctests:
        test_result = doctest.testmod()
        print(test_result)
